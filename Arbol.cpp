#include <fstream>
#include <iostream>
using namespace std;
#include <string>

// nodo
#include "programa.h"

//grafo
#include "Grafo.h"

//arbol
#include "Arbol.h"

// crea un arbol con su raiz, y dentro de la raiz están el resto de los nodos
Arbol::Arbol(){
  // raiz del arbol
  Nodo *raiz = NULL;

  // creación de arbol por defecto
  raiz = crearNodo(120, NULL);
  raiz->izq = crearNodo(111, raiz);
  raiz->der = crearNodo(133, raiz);

  raiz->izq->izq = crearNodo(100, raiz->izq);
  raiz->izq->der = crearNodo(116, raiz->izq);

  this->raiz = raiz;
}


// crea un nuevo nodo
Nodo* Arbol::crearNodo(int dato, Nodo *dad){
  Nodo *q;
  q = new Nodo(); //nodo nuevo
  q->izq = NULL; // como es nuevo no tiene nada la izquierda
  q->der = NULL; // como es nuevo no tiene nada la derecha
  q->info = dato; // su info será el dato que llega
  q->dad = dad; // nodo padre, cuando es el primero llega como "NULL"
  return q;
}


// busca el valor a agregar por si ya está dentro del arbol
void Arbol::verificar_repeticion(Nodo *p, int dato, bool &repetido){
  /*constantemente evalua si el nodo evaluando existe y si es igual al dato
    en el caso que se encuentre que está repetido el parametro repetido
    por referencia cambia a true.
    Si el parámetro no ha cambiado a true se evalúan las ramas*/
  if (p != NULL) {
    if(p->info == dato){
      repetido = true;
    }else{
      if (p->izq != NULL) {
        if(p->izq->info == dato){
          repetido = true;
        }
      }
      if (p->der != NULL) {
        if(p->der->info == dato){
            repetido = true;
        }
      }
    }
    if(repetido == false){
      verificar_repeticion(p->izq, dato, repetido);
      verificar_repeticion(p->der, dato, repetido);
    }
  }
}

void Arbol::add_node(Nodo *p, int dato){
  /*si el dato a ingresar es mayor que evaluado se revisa su rama derecha
    si su lado derecho está vacío se agrega un nodo
    si existe una hoja ya se evalúa la disponibilidad en la hoja evaluada*/
  if(dato > p->info){
    if(p->der == NULL){
      p->der = this->crearNodo(dato, p);
    }else{
      this->add_node(p->der, dato);
    }
  }
  /*si el dato a ingresar no es mayor entonces es menor ya que no puede estar repetido
    si en la izquierda hay espacio disponible se agrega
    sino se evalúa la rama izquierda hasta encontrar disponibilidad*/
  else{
    if(p->izq == NULL){
      p->izq = this->crearNodo(dato, p);
    } else{
      this->add_node(p->izq, dato);
    }
  }
}
// función que añade un nodo, rama, al árbol
void Arbol::agregar_nodo(int dato){
  Nodo *p = this->raiz;
  this->add_node(p, dato);
}

// reemplaza los nodos
void Arbol::reemplazar(Nodo *p, Nodo *reemplazo){
  // si el nodo tiene un nodo padre
  if(p->dad){
    // si la info del nodo es igual a la del nodo izquierdo del padre
    if(p->info == p->dad->izq->info){
      // el nodo izquierdo del padre será el enviado como reemplazo
      p->dad->izq = reemplazo;
    }
    // si la info del nodo es igual a la del nodo derecho del padre
    else if(p->info == p->dad->der->info){
      // el nodo derecho del padre será el enviado como reemplazo
      p->dad->der = reemplazo;
    }
  }
  // si el remplazo enviado no es nulo se cambia el nodo padre
  if(reemplazo){
    reemplazo->dad = p->dad;
  }
}

// función que retorna el nodo más pequeño "el izquierdo final de la rama"
Nodo* Arbol::izquierdo_final(Nodo *p){
  // si existe el nodo izquierdo se llama recursivamente
  if(p->izq){
    return izquierdo_final(p->izq);
  }
  // si no existe el nodo izquierdo entonces el que estamos evaluando es el último
  else{
    return p;
  }
}

// función que elimina el nodo
void Arbol::eraseNodo(Nodo *p){
  // si existen ambos nodos debe ir por el lado mayor para reemplazar el nodo al eliminar
  if(p->izq && p->der){
    // se obtiene el más pequeño de todos los mayores al nodo que se desea eliminar
    Nodo *izq = this->izquierdo_final(p->der);
    // se realiza un cambio donde el nodo tendrá ahora el número encontrado
    p->info = izq->info;
    // se elimina el número por el cual fue reemplazado
    this->eraseNodo(izq);
  }
  // si no existen los dos y solo existe el izquierdo
  else if(p->izq){
    // se reemplaza el nodo por el izquierdo y se elimina (se hace nulo)
    this->reemplazar(p, p->izq);
    p->izq = NULL;
  }
  // si no existen los dos y solo existe el derecho
  else if(p->der){
    // se reemplaza el nodo por el derecho y se elimina (se hace nulo)
    this->reemplazar(p, p->der);
    p->der = NULL;
  }
  // si simplemente no tiene hijos se reemplaza por nulo, se elimina
  else{
    this->reemplazar(p, NULL);
  }
}

void Arbol::crear_grafo(){
  Grafo *g = new Grafo(this->raiz);
}
