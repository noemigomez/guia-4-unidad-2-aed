#include <fstream>
#include <iostream>
using namespace std;
#include <string>

#ifndef ARBOL_H
#define ARBOL_H

class Arbol{
  private:

  public:
      // posee raices y nodos
      Nodo *raiz;
      /*
      Nodo *nodo;*/

      //constructor
      Arbol();

      Nodo *crearNodo(int dato, Nodo *dad);
      void verificar_repeticion(Nodo *p, int dato, bool &repetido);
      void add_node(Nodo *p, int dato);
      void agregar_nodo(int dato);
      void reemplazar(Nodo *p, Nodo *reemplazo);
      Nodo* izquierdo_final(Nodo *p);
      void eraseNodo(Nodo *p);
      void crear_grafo();
};
#endif
