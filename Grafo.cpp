#include <fstream>
#include <iostream>
using namespace std;
#include <string>

//nodo
#include "programa.h"

// grafo
#include "Grafo.h"

/*crea el archivo .txt de salida y
  le agrega contenido del árbol para generar el grafo (imagen).*/

Grafo::Grafo(Nodo *nodo){
  ofstream fp;

  //abre archivo
  fp.open ("grafo.txt");

  fp << "digraph G {" << endl;
  fp << "node [style=filled fillcolor=pink];" << endl;

  recorrer(nodo, fp);

  fp << "}" << endl;

  //cierra archivo
  fp.close();
  //genera grafo
  system("dot -Tpng -ografo.png grafo.txt &");

  //visualiza grafo
  system("eog grafo.png &");
}


//recorre en árbol en preorden y agrega datos al archivo.
void Grafo::recorrer(Nodo *p, ofstream &fp){
  if (p != NULL) {
    if (p->izq != NULL) {
      fp <<  p->info << "->" << p->izq->info << ";" << endl;
    } else {
      string cadena = std::to_string(p->info) + "i";
      fp << "\"" << cadena << "\"" << " [shape=point];" << endl;
      fp << p->info << "->" << "\"" << cadena << "\"" << ";" << endl;
    }

    if (p->der != NULL) {
      fp << p->info << "->" << p->der->info << ";" << endl;
    } else {
      string cadena = std::to_string(p->info) + "d";
      fp << "\"" <<  cadena << "\"" << " [shape=point];" << endl;
      fp << p->info << "->" << "\"" << cadena << "\"" << ";" << endl;
    }

    recorrer(p->izq, fp);
    recorrer(p->der, fp);
  }
}
