#include <fstream>
#include <iostream>
using namespace std;
#include <string>

#ifndef GRAFO_H
#define GRAFO_H

class Grafo {
    private:

    public:
        //constructor que construye el grafo y el archivo txt
        Grafo(Nodo *nodo);

        //recorre arbol y agrega datos al archivo txt
        void recorrer(Nodo *p, ofstream &fp);
};
#endif
