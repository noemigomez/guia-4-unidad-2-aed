# Árbol Binario de Búsqueda

El programa crea un árbol binario de búsqueda con 5 nodos de referencia, con ello su raíz. Permite posteriormente la manipulación del árbol con las opciones de insertar una hoja o nodo, eliminar una hoja o nodo, modificar, mostrar los datos del árbol, generar un gráfico (el cual crea el grafo en un archivo de texto y abre una fotografía con el gráfico creado) y salir.

Un árbol binario de búsqueda es un árbol binario, que posee solo una raíz y dos hijos cada nodo, y que posee solo números. Estos se ordenan de la manera que todo número menor que el nodo va en su rama izquierda y todo número mayor va en su rama derecha.

## Comenzando

Lo primero que el programa realiza es la creación del árbol. Se crea un nodo raíz con sus dos hijos y su hoja izquierda presenta también dos hojas hijas. De manera posterior se despliega un menú que presenta las 6 opciones señaladas en un inicio: insertar, agregar un nodo; eliminar, eliminar un nodo; modificar, eliminar un nodo viejo y agregar uno nuevo; mostrar, enseña los datos guardados hasta el momento en 3 ordenes distintos (preorden, inorden, postorden); generar grafo, el programa crea un archivo de texto con los datos para crear un grafo que es una visualización del árbol binario de búsqueda dando como resultado un archivo txt y se abre automáticamente la foto generada; finalmente la opción de salir del programa.


## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ Programa.cpp Arbol.cpp Grafo.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa`

El programa comienza a correr creando el árbol binario de búsqueda con una clase, siendo manipulado como un objeto, con 5 nodos, entre ellos su raíz. Luego despliega un menú que se repite constantemente hasta que, al momento de elegir una de las opciones que da, elija la opción salir. Las opciones son las siguientes:

**Opción 1, Insertar:** Esta opción comienza cuando se elige el número 1. Se pregunta cual es el número que desea agregar al árbol. Cuando se inserta el número se revisa en una función si ese número ya está en el árbol, si es que se encuentra el nodo no se agrega y si el número no está repetido entonces el nodo se agrega. No es válido el ingreso de un número no entero o de caracteres. Esto generará un ciclo infinito y el programa dejará de funcionar. Posterior al agregar el nodo se vuelve al menú para seguir manipulando el árbol binario de búsqueda.

**Opción 2, Eliminar:** Esta opción comienza al ingresar el número 2. Se pregunta cual es el número del árbol que desea eliminar. Si el número no se encuentra, no se elimina nada. Si se encuentra el número entonces del árbol dejando a uno de sus hijos si es que tiene. Se vuelve al menú. Si lo ingresado no es un número entero el programa se descompone entrando en un ciclo infinito.

**Opción3, Modificar:** Esta opción comienza ingresando el número 3. Se pregunta en primera instancia cual es el nodo que desea eliminar y luego por el cual lo desea reemplazar. La modificación es una eliminación y posterior inserción del nodo, se elimina el nodo y se inserta uno nuevo donde le corresponda ya que debe mantener un orden. La modificación será exitosa cuando el nodo ingresado a eliminar exista dentro del árbol y cuando el que se desea agregar no esté repetido. Si estas dos condiciones no se cumplen no se va a modificar el árbol. Se vuelve al menú. Si lo que se ingresa no es un número entero el programa se descompone entrando en un ciclo infinito.

**Opción 4, Mostrar contenido:** Esta opción comienza ingresando el número 4. Automáticamente se muestra el contenido del árbol en 3 órdenes diferentes: preorden, mostrando de la raíz recorriendo primero la parte izquierda y luego la parte derecha; inorden, comienza desde el menor nodo primero a la izquierda así hasta llegar al último nodo a la derecha; posorden, comienzan con los nodos hijos hasta el nodo raíz. Se vuelve al menú.

**Opción 5, Generar grafo:** Esta opción se activa insertando el número 5. Una función se encarga de crear un archivo txt con toda la información necesaria para crear una imagen gráfica del árbol con sus hijos. Crea el archivo txt, genera la imagen de extensión .png y la abre. Vuelve al menu.

**Opción otro, Salir:** Esta opción es válida con cualquier número entero que no sea uno de los anteriores. Termina el programa.

En el menú si se ingresa un dato que no sea un número entero, como números racionales o caracteres, genera una descomposición del programa en un ciclo infinito. La manera de salir de esto es con las teclas `ctrl+c`. En cualquier punto del programa al hacer esa combinación de teclas este termina.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream, string, fstream.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl
