#include <fstream>
#include <iostream>
using namespace std;
#include <string>

// nodo
#include "programa.h"

// grafo
//#include "Grafo.h"

// arbol
#include "Arbol.h"


// imprime el arbol en posorden
void posorden(Nodo *p){
  if(p != NULL){
    posorden(p->izq);
    posorden(p->der);
    cout << "|" << p->info << "|";
  }
}

// imprime el arbol en inorden
void inorden(Nodo *p){
  if(p != NULL){
    inorden(p->izq);
    cout << "|" << p->info << "|";
    inorden(p->der);
  }
}

// imprime el arbol en preorden
void preorden(Nodo *p){
  if(p != NULL){
    cout << "|" << p->info << "|";
    preorden(p->izq);
    preorden(p->der);
  }
}

// muestra el árbol en 3 tipos de ordenamientos
void mostrar_arbol(Nodo *raiz){
  cout << "\n----PREORDEN----" << endl;
  preorden(raiz);
  cout << "\n----INORDEN----" << endl;
  inorden(raiz);
  cout << "\n----POSORDEN----" << endl;
  posorden(raiz);
  cout << "\n";
}

/*aqui iban reemplaza, izquierdo final, eraseNodo*/

// función que busca un nodo a eliminar
void eliminar(Arbol *abb, Nodo *p, int dato){
  // si el nodo existe
  if(p != NULL){
    /*si es mayor o menor es porque el dato no se ha encontrado, el que se desea eliminar
      por lo tanto se llama de manera recursiva con el que corresponde (izq si es menor, der si es mayor)*/
    if(p->info > dato){
      eliminar(abb, p->izq, dato);
    }else if(dato > p->info){
      eliminar(abb, p->der, dato);
    // si no es mayor o menor que el nodo entonces se encuentra el que debe de eliminarse
    }else{
      abb->eraseNodo(p);
    }
  }
  // si no existe se revisó toda la rama y no se encontró
  else{
    cout << "Dato no encontrado" << endl;
  }
}


// función que inserta un número al árbol
void insertar(Arbol *abb, int dato){
  if(dato){
    // boleano para verificar que si el dato está repetido
    bool repetido = false;
    abb->verificar_repeticion(abb->raiz, dato, repetido);

    // si no está repetido (false) se agrega un nodo
    if(repetido == false){
      //abb->agregar_nodo(abb->raiz, dato);
      abb->agregar_nodo(dato);
    // si está repetido no se agrega
    }else{
      cout << "Ese número ya se encuentra en el árbol" << endl;
    }
  }
}

void modificar(Arbol *abb){
  int nodo_modifcar, nodo_reemplazo;
  cout << "Ingrese el nodo a modificar: ";
  cin >> nodo_modifcar;

  bool se_elimina = false;
  abb->verificar_repeticion(abb->raiz, nodo_modifcar, se_elimina);

  cout << "Ingrese número con el cual se reemplazará al nodo:";
  cin >> nodo_reemplazo;

  bool se_reemplaza = false;
  abb->verificar_repeticion(abb->raiz, nodo_reemplazo, se_reemplaza);

  /*se_elimina es true cuando se encuentra el nodo que se quiere eliminar
    se_reemplaza es falso cuando no se encuentra repetido por el que se quiere reemplazar
    esto se hace debido a que si se ingresa uno que se pueda eliminar pero se desea reemplazar
    por uno que existe, sin esta condición el otro nodo se eliminaría igual, o se puede agregar el nodo
    pero no eliminar, por lo tanto se necesitan ambas condiciones para que realmente sea modificacion */
  if(se_elimina == true && se_reemplaza == false){
    eliminar(abb, abb->raiz, nodo_modifcar);
    insertar(abb, nodo_reemplazo);
  }else{
    if(se_reemplaza){
      cout << "El valor está repetido, no se puede modificar el nodo" << endl;
    }
    if(se_elimina == false){
      cout << "El valor que desea eliminar no se encuentra, no se puede modificar el nodo" << endl;
    }
  }
}

void dato_adecuado(int opcion, bool &validacion){
  // cuando lo ingresado no es entero, opcion es falso, se termina el ciclo
  if(opcion){
    validacion = true;
  }else{
    validacion = false;
  }
}

// menu
void menu(Arbol *abb){
  // creación de datos a manipular
  int opcion, dato;
  bool opcion_valida, dato_valido;
  do{
    cout << "--------------------------------" << endl;
    cout << "Escoja una opción" << endl;
    cout << "[1] Insertar " << endl;
    cout << "[2] Eliminar " << endl;
    cout << "[3] Modificar" << endl;
    cout << "[4] Mostrar contenido actual" << endl;
    cout << "[5] Generar grafo" << endl;
    cout << "[Otro número] Salir" << endl;
    cout << "Ingrese: ";
    cin >> opcion;

    dato_adecuado(opcion, opcion_valida);
    if(opcion_valida){
      // opcion ingresada
      switch (opcion) {
        case 1:
        cout << "Ingrese número a agregar: ";
        cin >> dato;
        insertar(abb, dato);
        break;

        case 2:
        cout << "Ingrese número a eliminar: ";
        cin >> dato;
        eliminar(abb, abb->raiz, dato);
        break;

        case 3:
        modificar(abb);
        break;

        case 4:
        mostrar_arbol(abb->raiz);
        break;

        case 5:
        abb->crear_grafo();
        break;

        default:
        break;
      }
    }else{
      break;
    }

  }while(opcion < 6 && opcion > 0);
}


// función principal
int main(void) {
  Arbol *abb = new Arbol();
  menu(abb);

  return 0;
}
