#include <fstream>
#include <iostream>
using namespace std;

typedef struct _Nodo {
    // información del nodo
    int info;
    struct _Nodo *izq; //hoja izquierda
    struct _Nodo *der; //hoja derecha
    struct _Nodo *dad; //hoja padre
} Nodo;
